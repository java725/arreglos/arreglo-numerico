package com.arreglos;

import java.util.Scanner;

public class Main
{
    private static final Double[] arreglo = new Double[4];

    public static void main(String[] args) {
        llenar();
        mostrar();
    }

    private static void llenar() {
        Scanner entrada = new Scanner(System.in);
        for (int indice = 0; indice < arreglo.length; indice++) {
            System.out.println("Ingrese el numero " + (indice + 1) + ":");
            arreglo[indice] = Double.parseDouble(entrada.nextLine());
        }
        entrada.close();
    }

    private static void mostrar() {
        System.out.println("El arreglo es: ");
        for (Double numero: arreglo) {
            System.out.println(numero);
        }
    }
}